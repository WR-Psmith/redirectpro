<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Untitled Document</title>
</head>

<body>
	<form action="" method="get">
		<fieldset>
			URL to test:<br>
			<input name="url" type="text">
			<input type="submit" value="Submit">
		</fieldset>
	</form>
	
	
	<?php
	// Set delimiter (could be a DB config but / is the most common for regex).
	$delimiter = "/";
	// have you got a URL?
	if (isset ($_GET['url'])) {
		$source = $_GET['url'];
		// Connect to Database
		require_once('../../includes/dbconnect.php');
		// Create query
		$sql = "SELECT regex_source, redirect_destination, regex_sort_order FROM redirector.regex_redirects ORDER BY regex_sort_order ASC";
		// Get results
		$result = $conn->query($sql);
		// If we have some results
		if ($result->num_rows > 0) {
			// Close connection, we are done.
			$conn->close();
			echo "<h1>Matched Results (would be applied in order):</h1>";
			// output data of each row
			while($row = $result->fetch_assoc()) {
				// Create the Regex string
				$pattern = $delimiter . $row['regex_source'] . $delimiter;
					// Match the url to the regex results
					if (preg_match($pattern, $source)) {
						// display these results for now, but when we generate the appropriate header, the user will be taken away from the page so the remainder will not apply.
						echo "Sort Order: " . $row['regex_sort_order'] . " - Regex: " . $row["regex_source"]. " - Destination: " . $row["redirect_destination"]. " ";
						echo "-- A match was found. Redirect could be applied. <br>";
					}
					else {
						// Nothing, we dont want to see these as they dont match.
					}
			}
		} else {
			//close connection, we are done.
			$conn->close();
			echo "0 results";
		}
		
	} else {
		// nothing
	}
	
	?>
</body>
</html>