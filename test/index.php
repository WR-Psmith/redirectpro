<!doctype html>
<html>

<head>
	<?php
		 header("Cache-Control: max-age=300");
	?>
	<meta charset="utf-8">
	<title>Untitled Document</title>

</head>

<body>
<form action="" method="get">
	<fieldset>
	URL to test:<br>
	<input name="url" type="text">
	<input type="submit" value="Submit">
	</fieldset>
</form>
<?php

	if (isset($_GET["url"]) ) {
		$actual_link = $_GET["url"];

		if ( isset( $actual_link ) ) {
			
			echo "Looking for redirects for: " . $actual_link . "</br>";
			// Create connection
			require_once( '../includes/dbconnect.php' );
			// Check connection
			if ($conn->connect_error) {
				die("Connection to Database failed: " . $conn->connect_error) . "<br>";
			}
			echo "Connection to Database successful <br>";
			
			// Look for a matching source
				// TO DO - Build in Regex check against $actual_link from Source in DB
			$sql = "SELECT redirects.redirect_destination, header_types.type_code, header_types.type_render_text, header_types.type_html_page, header_types.type_render_page, redirects.redirect_status
						FROM redirector.redirects
							LEFT JOIN redirector.header_types ON redirects.redirect_type_id = header_types.type_id
    					WHERE redirects.redirect_status = 1 AND redirects.redirect_source = '" . $actual_link . "';";
			// Get results
			$result = $conn->query( $sql );
			// Check we have 1 record matched
			if ( $result->num_rows == 1) {
				// Get the row details
				while ( $row = $result->fetch_assoc() ) {
					$destination = $row[ "redirect_destination" ];
					$redirect_type = $row[ "type_code" ];
					$redirect_description = $row[ "type_render_text" ];
					$page_html = $row[ "type_html_page" ];
					$ispage = $row[ "type_render_page" ];
					// Is this a page?
					if ($ispage == 1){
						echo "This is a page render, not a redirect. The page will show below: <br>";
						echo 'You will be given a ' . $redirect_type . ' ' . $redirect_description . ' header.';
						header( "HTTP/1.1 $redirect_type $redirect_description" );
						echo $page_html;
					}
					else {
						echo 'You will be redirected to: ' . $destination . ' using a ' . $redirect_type . ' ' . $redirect_description . ' redirect header.';
						// Create the HTTP Header for the Redirect
						// header( "HTTP/1.1 $redirect_type $redirect_description" );
						// header( "Location: $destination" );
						echo '<br><a href="' . $actual_link . '" target="_blank">Try this redirect</a>';
					}
				}
			// If we do not have 1 record, show error (as which would we choose, if any are found)
				// TO DO - Build in Regex check to return best match instead
			} else {
				echo "ERROR: We found <strong>" . $result->num_rows . "</strong> possible redirects";
			}
			$conn->close();
			// If you come from another source and somehow this doesn't work, lets 403 you to prevent any kind of DDOS on the Database.
		} else {
			header( 'HTTP/1.1 404 Page not found' );
			echo "<h1>404 Page not found</h1>";
			echo "<p>Please check your URL and try again.</p>";
			exit();
		}
	}
	else {
		// do nothing
	}

?>
</body>

</html>