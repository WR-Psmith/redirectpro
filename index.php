<!doctype html>
<html>

<head>
	<?php
		header( "Cache-Control: max-age=300" );
	?>
<meta charset="utf-8">
<title>Untitled Document</title>

</head>

<body>

	<?php
	// check if HTTP_HOST is set
	if ( isset( $_SERVER[ 'HTTP_HOST' ] ) ) {
		// what URL have you requested?
		$url = stripos($_SERVER['SERVER_PROTOCOL'],'https') === true ? 'https:' : 'http:' . "//$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		// Remove trailing slash (REGEX equivilant: /?)
		$actual_link = rtrim($url,"/");
		
		// Once we have a clean link, lets get to it.
		if ( isset( $actual_link ) ) {
			
			// Create connection do the Database
			require_once( 'includes/dbconnect.php' );
						
			// Look for a matching rule in the database
				// TO DO - Build in Regex check against $actual_link from Source in DB, allowing regex rules in the database to be applied on best fir to the incoming URL.
			$sql = "SELECT redirects.redirect_destination, header_types.type_code, header_types.type_render_text, header_types.type_html_page, header_types.type_render_page, redirects.redirect_status
						FROM redirector.redirects
							LEFT JOIN redirector.header_types ON redirects.redirect_type_id = header_types.type_id
    					WHERE redirects.redirect_status = 1 AND redirects.redirect_source = '" . $actual_link . "';";
			// Get results of the Query
			$result = $conn->query( $sql );
			// Check we have 1 record matched
			if ( $result->num_rows == 1) {
				// Get the row details
				while ( $row = $result->fetch_assoc() ) {
					$destination = $row[ "redirect_destination" ];
					$redirect_type = $row[ "type_code" ];
					$redirect_description = $row[ "type_render_text" ];
					$page_html = $row[ "type_html_page" ];
					$ispage = $row[ "type_render_page" ];
					// Is this a page?
					if ($ispage == 1){
						// YES, create the page and header
						header( "HTTP/1.1 $redirect_type $redirect_description" );
						echo $page_html;
					}
					else {
						// NO, Create the HTTP Header for the Redirect
						header( "HTTP/1.1 $redirect_type $redirect_description" );
						header( "Location: $destination" );
					}
				}
			// If we do not have 1 record, show error (as which would we choose, if any are found)
				// TO DO - Build in Regex check to return best match instead
			} else {
				echo "ERROR: We found <strong>" . $result->num_rows . "</strong> possible redirects for: ";
				echo $actual_link;
			}
			$conn->close();
			// If you come from another source and somehow this doesn't work, lets 404 you to prevent any kind of oddness.
		} else {
			header( 'HTTP/1.1 404 Page not found' );
			echo "<h1>404 Page not found</h1>";
			echo "<p>Please check your URL and try again.</p>";
			exit();
		}
	}

	// If HTTP_HOST not set, generate a 403 as you are doing something dodgy.
	else {
		header( 'HTTP/1.1 403 Forbidden' );
		echo "<h1>403 Forbidden</h1>";
		echo "You are not authorised to access this resource.";
		exit();
	}

	?>
</body>

</html>